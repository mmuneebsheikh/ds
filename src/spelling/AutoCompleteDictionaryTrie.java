package spelling;

import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * An trie data structure that implements the Dictionary and the AutoComplete ADT
 * @author You
 *
 */
public class AutoCompleteDictionaryTrie implements  Dictionary, AutoComplete {

    private TrieNode root;
    private int size;


    public AutoCompleteDictionaryTrie()
	{
		root = new TrieNode();
	}


	/** Insert a word into the trie.
	 * For the basic part of the assignment (part 2), you should convert the
	 * string to all lower case before you insert it.
	 *
	 * This method adds a word by creating and linking the necessary trie nodes
	 * into the trie, as described outlined in the videos for this week. It
	 * should appropriately use existing nodes in the trie, only creating new
	 * nodes when necessary. E.g. If the word "no" is already in the trie,
	 * then adding the word "now" would add only one additional node
	 * (for the 'w').
	 *
	 * @return true if the word was successfully added or false if it already exists
	 * in the dictionary.
	 */
	public boolean addWord(String word)
	{
	    //TODO: Implement this method.
		char[] letters = word.toLowerCase().toCharArray();
		TrieNode node = this.root;
		for(int i = 0; i < letters.length; i++){
			node.insert(letters[i]);
			node = node.getChild(letters[i]);
//			System.out.print(node.getText());
		}
		node.setEndsWord(true);
	    return false;
	}

	/**
	 * Return the number of words in the dictionary.  This is NOT necessarily the same
	 * as the number of TrieNodes in the trie.
	 */
	public int size()
	{
	    //TODO: Implement this method
		this.size(this.root);
		return this.size;

	}
	private void size(TrieNode curr){
		if (curr == null)
 			return;

		if(curr.endsWord()){
			this.size++;
		}
// 		System.out.println(curr.isWord + ", " + curr.getText());
// 		System.out.println(curr.getValidNextCharacters());

 		TrieNode next = null;
 		for (Character c : curr.getValidNextCharacters()) {
 			next = curr.getChild(c);
 			size(next);
 		}
	}

	/** Returns whether the string is a word in the trie, using the algorithm
	 * described in the videos for this week. */
	@Override
	public boolean isWord(String s)
	{
	    // TODO: Implement this method
		char[] letters = s.toLowerCase().toCharArray();
		TrieNode n = this.root;
		try{
			for(int ind = 0; ind < letters.length; ind++){
				 n = n.getChild(letters[ind]);
			}
			return n.endsWord();
		}
		catch(NullPointerException e){
			return false;
		}
	}

	/**
     * Return a list, in order of increasing (non-decreasing) word length,
     * containing the numCompletions shortest legal completions
     * of the prefix string. All legal completions must be valid words in the
     * dictionary. If the prefix itself is a valid word, it is included
     * in the list of returned words.
     *
     * The list of completions must contain
     * all of the shortest completions, but when there are ties, it may break
     * them in any order. For example, if there the prefix string is "ste" and
     * only the words "step", "stem", "stew", "steer" and "steep" are in the
     * dictionary, when the user asks for 4 completions, the list must include
     * "step", "stem" and "stew", but may include either the word
     * "steer" or "steep".
     *
     * If this string prefix is not in the trie, it returns an empty list.
     *
     * @param prefix The text to use at the word stem
     * @param numCompletions The maximum number of predictions desired.
     * @return A list containing the up to numCompletions best predictions
     */@Override
     public List<String> predictCompletions(String prefix, int numCompletions)
     {
    	 // TODO: Implement this method
    	 // This method should implement the following algorithm:
    	 // 1. Find the stem in the trie.  If the stem does not appear in the trie, return an
    	 //    empty list
    	 // 2. Once the stem is found, perform a breadth first search to generate completions
    	 //    using the following algorithm:
    	 //    Create a queue (LinkedList) and add the node that completes the stem to the back
    	 //       of the list.
    	 //    Create a list of completions to return (initially empty)
    	 //    While the queue is not empty and you don't have enough completions:
    	 //       remove the first Node from the queue
    	 //       If it is a word, add it to the completions list
    	 //       Add all of its child nodes to the back of the queue
    	 // Return the list of completions
    	 List<String> completions = new LinkedList<String>();
    	 Queue<TrieNode> q = new LinkedList<TrieNode>();
    	 TrieNode node = this.root;
    	 int words = 0;
    	 //find stem
    	 char[] letters = prefix.toLowerCase().toCharArray();
    	 for(int i=0;i<prefix.length();i++){
    		 Set<Character> validChilds = node.getValidNextCharacters();
    		 if(!validChilds.contains(letters[i])){
    			 // stem not found
    			 return Collections.emptyList();
    		 }
    		 node = node.getChild(letters[i]);
    	 }
    	 // node is at stem
    	 q.add(node);
    	 while(!q.isEmpty() && (completions.size() < numCompletions)){
    		 TrieNode curr = q.remove();
//    		 System.out.println(curr.getValidNextCharacters());
    		 if(curr.endsWord()){
    			 completions.add(curr.getText());
    		 }
    		 Set<Character> validChilds = curr.getValidNextCharacters();
    		 for(char child : validChilds){
    			 q.add(curr.getChild(child));
    		 }
    	 }
         return completions;
     }

 	// For debugging
 	public void printTree()
 	{
 		printNode(root);
 	}

 	/** Do a pre-order traversal from this node down */
 	public void printNode(TrieNode curr)
 	{
 		if (curr == null)
 			return;

 		System.out.println(curr.getText());
// 		System.out.println(curr.getValidNextCharacters());

 		TrieNode next = null;
 		for (Character c : curr.getValidNextCharacters()) {
 			next = curr.getChild(c);
 			printNode(next);
 		}
 	}
 	public int testing(){
 		TrieNode node = this.root;
		Set<Character> firstLetters = node.getValidNextCharacters();
		char[] visited = new char[firstLetters.size()];
		for(char alph : firstLetters){
			node = node.getChild(alph);
			if(node.endsWord()){

			}
			TrieNode n = node.getChild(alph);
			System.out.println("parent: "+n.getText());
			System.out.println("child: "+n.getValidNextCharacters());
		}
	    return 0;
 	}
 	public static void main(String args[]) {

 		AutoCompleteDictionaryTrie objAuto = new AutoCompleteDictionaryTrie();

 		objAuto.addWord("apple");

// 		objAuto.printTree();
 		objAuto.addWord("apps");
 		System.out.println("Added...");
// 		objAuto.printTree();
 		objAuto.addWord("allow");
// 		System.out.println("Added...");
// 		objAuto.printTree();
// 		System.out.println(objAuto.root.getValidNextCharacters());
 		System.out.println(objAuto.size());
 		// 		objAuto.testing();
 		// 		if (objAuto.isWord("apple")) {
// 			System.out.print("found");
//		}
// 		else{
// 			System.out.print("notfound");
// 		}
 	}


}