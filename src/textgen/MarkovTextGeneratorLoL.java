package textgen;

import java.lang.reflect.Constructor;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

/**
 * An implementation of the MTG interface that uses a list of lists.
 * @author UC San Diego Intermediate Programming MOOC team
 */
public class MarkovTextGeneratorLoL implements MarkovTextGenerator {

	// The list of words with their next words
	private List<ListNode> wordList;

	// The starting "word"
	private String starter;

	// The random number generator
	private Random rnGenerator;

	private void init(Random generator){
		wordList = new LinkedList<ListNode>();
		starter = "";
		rnGenerator = generator;
	}
	public MarkovTextGeneratorLoL(Random generator)
	{
		init(generator);
	}


	/** Train the generator by adding the sourceText */
	@Override
	public void train(String sourceText)
	{
		// TODO: Implement this method
		String[] words = sourceText.split(" +");
		this.starter = words[0];
		String prevWord = this.starter;
		for(int i = 1; i < words.length; i++){
			boolean isAdded = false;
//			System.out.println(i);
			for(ListNode node : this.wordList){
				if(prevWord.equals(node.getWord())){
//					System.out.println("isAdded: true -> " + prevWord + ":" + words[i]);
					isAdded = true;
					node.addNextWord(words[i]);
					break;
				}
			}
			if(!isAdded){
//				System.out.println("isAdded: false -> " + prevWord + ":" + words[i]);
				ListNode newNode = new ListNode(prevWord);
				newNode.addNextWord(words[i]);
				this.wordList.add(newNode);
			}

			prevWord = words[i];
		}
		if(this.wordList.size() <= 0){return;}
		ListNode node = this.wordList.get(this.wordList.size()-1);
		if(prevWord.equals(node.getWord())){
//			System.out.println("LastisAdded: true -> " + prevWord + ":" + starter);
			node.addNextWord(starter);
		}
		else{
//			System.out.println("LastisAdded: false -> " + prevWord + ":" + starter);
			ListNode newNode = new ListNode(prevWord);
			newNode.addNextWord(starter);
			this.wordList.add(newNode);
		}
//		ListNode n = this.wordList.get(this.wordList.size()-1).addNextWord(starter);
	}

	/**
	 * Generate the number of words requested.
	 */
	@Override
	public String generateText(int numWords) {
	    // TODO: Implement this method
		String currWord = this.starter;
		String output = "";
//		System.out.println(output);
		int wordsNeeded = numWords;
		while(wordsNeeded > 0){
			for(ListNode node : this.wordList){
				if(node.getWord().equals(currWord)){
					output += currWord + " ";
					String w = node.getRandomNextWord(rnGenerator);
//					output += " " + w;
					currWord = w;
					break;
				}
			}
			wordsNeeded--;
		}
//		for(int wordsAdded = 1; wordsAdded < numWords; wordsAdded++){
//			for(ListNode node : this.wordList){
//				if(node.getWord().equals(currWord)){
//					String w = node.getRandomNextWord(rnGenerator);
//					output += " " + w;
//					currWord = w;
//					break;
//				}
//			}
//		}
		return output.trim();
	}


	// Can be helpful for debugging
	@Override
	public String toString()
	{
		String toReturn = "";
		for (ListNode n : wordList)
		{
			toReturn += n.toString();
		}
		return toReturn;
	}

	/** Retrain the generator from scratch on the source text */
	@Override
	public void retrain(String sourceText)
	{
		// TODO: Implement this method.
		init(this.rnGenerator);
		this.train(sourceText);
	}

	// TODO: Add any private helper methods you need here.


	/**
	 * This is a minimal set of tests.  Note that it can be difficult
	 * to test methods/classes with randomized behavior.
	 * @param args
	 */
	public static void main(String[] args)
	{
		// feed the generator a fixed random value for repeatable behavior

		MarkovTextGeneratorLoL gen = new MarkovTextGeneratorLoL(new Random(42));

//		String text = "hi there hi leo";
//		gen.train(text);
//		System.out.println(gen.toString());
//		System.out.println(gen.generateText(1));
		String textString = "Hello.  Hello there.  This is a test.  Hello there.  Hello Bob.  Test again.";
		System.out.println(textString);
		gen.train(textString);
		System.out.println(gen);
		System.out.println(gen.generateText(20));
		String textString2 = "You say yes, I say no, "+
				"You say stop, and I say go, go, go, "+
				"Oh no. You say goodbye and I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello. "+
				"I say high, you say low, "+
				"You say why, and I say I don't know. "+
				"Oh no. "+
				"You say goodbye and I say hello, hello, hello. "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello. "+
				"Why, why, why, why, why, why, "+
				"Do you say goodbye. "+
				"Oh no. "+
				"You say goodbye and I say hello, hello, hello. "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello. "+
				"You say yes, I say no, "+
				"You say stop and I say go, go, go. "+
				"Oh, oh no. "+
				"You say goodbye and I say hello, hello, hello. "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello, hello, hello, "+
				"I don't know why you say goodbye, I say hello, hello, hello,";
		System.out.println(textString2);
		gen.retrain(textString2);
		System.out.println(gen);
		System.out.println(gen.generateText(20));
	}

}

/** Links a word to the next words in the list
 * You should use this class in your implementation. */
class ListNode
{
    // The word that is linking to the next words
	private String word;

	// The next words that could follow it
	private List<String> nextWords;

	ListNode(String word)
	{
		this.word = word;
		nextWords = new LinkedList<String>();
	}

	public String getWord()
	{
		return word;
	}

	public void addNextWord(String nextWord)
	{
		nextWords.add(nextWord);
	}

	public String getRandomNextWord(Random generator)
	{
		//TODO:
	    // The random number generator should be passed from
	    // the MarkovTextGeneratorLoL class
		int randNum = generator.nextInt(nextWords.size());
//		System.out.println("randNum: " + randNum + " length: " + nextWords.size() + " word: " + nextWords.get(randNum));
	    return nextWords.get(randNum);
	}

	public String toString()
	{
		String toReturn = word + ": ";
		for (String s : nextWords) {
			toReturn += s + "->";
		}
		toReturn += "\n";
		return toReturn;
	}

}


